package com.example.studycase3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    private TextView namaDetail, pekerjaanDetail;
    private ImageView fotoDetail;
    private int avatarCode;
    private String mNama,mPekerjaan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        namaDetail = findViewById(R.id.detail_Nama);
        pekerjaanDetail = findViewById(R.id.detail_Pekerjaan);
        fotoDetail = findViewById(R.id.imageView3);

        mNama = getIntent().getStringExtra("nama");
        mPekerjaan = getIntent().getStringExtra("pekerjaan");
        avatarCode = getIntent().getIntExtra("gender",2);

        namaDetail.setText(mNama);
        pekerjaanDetail.setText(mPekerjaan);
        switch (avatarCode){
            case 1 :
                fotoDetail.setImageResource(R.drawable.ic_boy);
                break;
            case 2 :
            default:
                fotoDetail.setImageResource(R.drawable.ic_girl);
                break;
        }
    }
}
