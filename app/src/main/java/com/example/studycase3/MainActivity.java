package com.example.studycase3;

import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {
    Dialog dialog;
    private RecyclerView mRecyclerView;
    private ArrayList<User> daftarUser;
    private AdapterUntukUser adapterUntukUser;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        mRecyclerView = findViewById(R.id.idRecyclerView);

        /*GridColumn buat ngatur jumlah kolomnya,
            Kenapa pake XML ? Karena nilainya bisa berubah sesuai Orientasi ataupun ketentuan lainnya
            Coba buka res->values Nah disitu nilai Integernya ada dua, yang biasa dan yang landscape nilainya berbeda
        */
        int gridColumnCount =getResources().getInteger(R.integer.grid_column_count);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this,gridColumnCount));


        daftarUser = new ArrayList<>();
        //Ngecek Apakah ada data Array di savedInstance
        if (savedInstanceState!=null){
            //Jika tidak null Maka balikin datanya
            daftarUser.clear();
            for (int i = 0; i <savedInstanceState.getStringArrayList("nama").size() ; i++) {
                daftarUser.add(new User(savedInstanceState.getStringArrayList("nama").get(i),
                        savedInstanceState.getStringArrayList("pekerjaan").get(i),
                        savedInstanceState.getIntegerArrayList("gender").get(i)));
            }
        }else {
            //Jika null balik dari awal
            init();
        }

        adapterUntukUser = new AdapterUntukUser(daftarUser,this);
        mRecyclerView.setAdapter(adapterUntukUser);

        //ItemTouchHelper biar bisa di swipe
        ItemTouchHelper helper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                int from = viewHolder.getAdapterPosition();
                int to = target.getAdapterPosition();
                Collections.swap(daftarUser, from ,to);
                adapterUntukUser.notifyItemMoved(from,to);

                return true;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                daftarUser.remove(viewHolder.getAdapterPosition());
                adapterUntukUser.notifyItemRemoved(viewHolder.getAdapterPosition());
            }
        });
        //Masukkan helper ke RecyclerView
        helper.attachToRecyclerView(mRecyclerView);

    }

    void init(){
        //Masukkan Data Dummy
        daftarUser.clear();
        //Dummmy Data
        daftarUser.add(new User("Athallariq","Android Developer",1));
        daftarUser.add(new User("Naura","Back End Developer",2));

    }

    //UNTUK MEMBUAT DIALOG/POPUP UNTUK TAMBAH USER
    void tambah(View view){
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.activity_adapter_untuk_user);
        final TextView mNama,mPekerjaan;
        final Spinner mGender;
        mNama = dialog.findViewById(R.id.edName);
        mPekerjaan = dialog.findViewById(R.id.edPekerjaan);

        TextView tambah=dialog.findViewById(R.id.txTambahUser);
        TextView batal = dialog.findViewById(R.id.txBatal);

        mGender = dialog.findViewById(R.id.spGender);

        String[]list={"Male","Female"};

        ArrayAdapter<String>adapterX = new ArrayAdapter(dialog.getContext(),android.R.layout.simple_spinner_item,list);
        mGender.setAdapter(adapterX);

        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                daftarUser.add(new User(mNama.getText().toString(),mPekerjaan.getText().toString(),mGender.getSelectedItemPosition()+1));
                adapterUntukUser.notifyDataSetChanged();
                dialog.dismiss();
            }
        });
        batal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //KARENA CUKUP RIBET UNTUK saveInstance ArrayAdapter JADI PERLU BREAKDOWN LAGI MASING MASING JADI ARRAYLIST
        //PERTAMA CONVERT JADI ARRAYLIST DULU YANG PASTI
        ArrayList<String>tempListNama = new ArrayList<>();
        ArrayList<String>tempListPekerjaan = new ArrayList<>();
        ArrayList<Integer>tempListGender = new ArrayList<>();
        for (int i = 0; i <daftarUser.size() ; i++) {
            tempListNama.add(daftarUser.get(i).getNama());
            tempListPekerjaan.add(daftarUser.get(i).getPekerjaan());
            tempListGender.add(daftarUser.get(i).getAvatar());
        }
        //BARU SIMPAN JADI OUTSTATE
        outState.putStringArrayList("nama",tempListNama);
        outState.putStringArrayList("pekerjaan",tempListPekerjaan);
        outState.putIntegerArrayList("gender",tempListGender);
        super.onSaveInstanceState(outState);

    }
}
