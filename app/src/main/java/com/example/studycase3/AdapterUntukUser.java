package com.example.studycase3;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class AdapterUntukUser extends RecyclerView.Adapter<AdapterUntukUser.ViewHolder> {
    private ArrayList<User> daftarUser;
    private Context mContext;

    public AdapterUntukUser(ArrayList<User> daftarUser, Context mContext) {
        this.daftarUser = daftarUser;
        this.mContext = mContext;
    }

    @Override
    public AdapterUntukUser.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(mContext).
                inflate(R.layout.activity_user, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(AdapterUntukUser.ViewHolder viewHolder, int urutan) {
        User currentUser = daftarUser.get(urutan);
        viewHolder.bindTo(currentUser);
    }

    @Override
    public int getItemCount() {
        return daftarUser.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView nama, pekerjaan;
        private ImageView foto;
        private int avatarCode;

        public ViewHolder(View itemView) {
            super(itemView);

            nama = itemView.findViewById(R.id.tx_nama);
            pekerjaan = itemView.findViewById(R.id.tx_pekerjaan);
            foto = itemView.findViewById(R.id.avatar);

            itemView.setOnClickListener(this);
        }

        void bindTo(User currentUser){
            nama.setText(currentUser.getNama());
            pekerjaan.setText(currentUser.getPekerjaan());

            avatarCode = currentUser.getAvatar();
            switch (currentUser.getAvatar()){
                case 1 :
                    foto.setImageResource(R.drawable.ic_boy);
                    break;
                case 2 :
                default:
                    foto.setImageResource(R.drawable.ic_girl);
                    break;
            }
        }

        @Override
        public void onClick(View v) {
            //KETIKA DI KLIK DAPAT MEMBUAT ACTIVITY DETAIL DENGAN BEBERAPA EXTRAS
            Intent toDetailActivity = new Intent(v.getContext(),DetailActivity.class);
            toDetailActivity.putExtra("nama",nama.getText().toString());
            toDetailActivity.putExtra("gender",avatarCode);
            toDetailActivity.putExtra("pekerjaan",pekerjaan.getText().toString());
            v.getContext().startActivity(toDetailActivity);
        }
    }

}
